// if you checked "fancy-settings" in extensionizr.com, uncomment this lines

// var settings = new Store("settings", {
//     "sample_setting": "This is how you use Store.js to remember values"
// });


chrome.runtime.onStartup.addListener(function () {
  console.log('open');
  loadDataFromWeb();
})

function log(msg, force) {
  if (true || force) {
    console.log(`Taylor's Version:`, msg);
  }
}


function checkForRedirects(ev) {
  if (ev.method != 'GET') return {};

}

function handleRedirects(ev) {
  log(ev);
  chrome.storage.local.get(/* String or Array */["tayUpdate", "tayRedirects", "enableTay"], function (items) {
    const itNow = new Date();
    const btwn = parseInt((itNow - Date.parse(items.tayUpdate)) / 1000)
    if (btwn > 300) {
      log('updating redirects');
      loadDataFromWeb();
    }

    let thisRedirect = items['tayRedirects'].find(e => e.from === ev.url)
    if (thisRedirect) {
      chrome.tabs.update(ev.tabId, { url: thisRedirect.to });
    }
  });

}

function loadDataFromWeb() {
  //https://gitlab.com/brendan-demo/taylorsbrowser/-/raw/main/redirects/redirects.csv
  var r = new XMLHttpRequest();
  r.open("GET", "https://brendan.fyi/taylorsv");
  r.onreadystatechange = function () {
    if (r.readyState != 4 || r.status != 200) return;
    chrome.storage.local.set({ "enableTay": true });
    chrome.storage.local.set({ "tayUpdate": Date() });
    chrome.storage.local.set({ "tayRedirects": csvToArray(r.responseText) });
  }
  r.send();
}

function csvToArray(str, delimiter = ",") {
  // slice from start of text to the first \n index
  // use split to create an array from string by delimiter
  const headers = str.slice(0, str.indexOf("\n")).split(delimiter);

  // slice from \n index + 1 to the end of the text
  // use split to create an array of each csv value row
  const rows = str.slice(str.indexOf("\n") + 1).split("\n");

  // Map the rows
  // split values from each row into an array
  // use headers.reduce to create an object
  // object properties derived from headers:values
  // the object passed as an element of the array
  const arr = rows.map(function (row) {
    const values = row.split(delimiter);
    const el = headers.reduce(function (object, header, index) {
      object[header] = values[index];
      return object;
    }, {});
    return el;
  });

  // return the array
  return arr;
}

chrome.webNavigation.onHistoryStateUpdated.addListener(handleRedirects);